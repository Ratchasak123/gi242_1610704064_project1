﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Manager
{
   public class ResultDialog : MonoBehaviour
   {

      private void OnRestartButtonClicked()
      {
         gameObject.SetActive(false);

         GameManager.Instance.StartGame();
      }
   }
}
